fn main() {
    println!("cargo:rustc-link-arg-bins=-nostartfiles");
    // println!("cargo:rustc-link-arg-bins=-no-pie");
    println!("cargo:rustc-link-arg-bins=-static-pie");
    println!("cargo:rustc-link-arg-bins=-shared");
    println!("cargo:rustc-link-arg-bins=-Wl,--version-script=version.script");
    // println!("cargo:rustc-link-arg-bins=-Wl,-z,defs");
}
