#![no_std]
#![feature(lang_items)]
#![no_main]
#![feature(const_maybe_uninit_zeroed)]

use core::arch::asm;
use core::mem::MaybeUninit;

extern "C" {
    fn _dl_tlsdesc_return(on_rax: *const tlsdesc) -> isize;
    // static __ehdr_start: u8; // emitted by linker
    // static _DYNAMIC: Elf64_Dyn; // emitted by linker, only when there is a dynamic section
    //                             // -no-pie: no dynamic section
    //                             // -static-pie: has a dynamic section
    //                             // default: has ld.so set as interp
}

/* The dynamic section is writable.  */
const DL_RO_DYN_SECTION: u8 = 0; // arch-dependent. 0 for x86_64.

#[inline]
unsafe fn elf_machine_load_address() -> Elf64_Addr {
    let mut addr: Elf64_Addr;
    asm!(
    "lea {addr}, [rip + __ehdr_start]",
    addr = out(reg) addr,
    );
    addr
}

#[inline]
unsafe fn elf_machine_dynamic() -> Elf64_Addr {
    let mut d: Elf64_Addr;
    asm!(
        "lea {d}, [rip + _DYNAMIC]",
        d = out(reg) d,
    );
    // (&_DYNAMIC as *const _ as Elf64_Addr) - elf_machine_load_address()
    d - elf_machine_load_address()
}

#[allow(non_camel_case_types)]
type Elf64_Sxword = i64;
#[allow(non_camel_case_types)]
type Elf64_Xword = u64;
#[allow(non_camel_case_types)]
type Elf64_Addr = u64;
#[allow(non_camel_case_types)]
type d_tag_utype = Elf64_Xword;
#[allow(non_camel_case_types)]
type Elf64_Word = u32;

const DT_NULL: usize = 0;
const DT_PLTRELSZ: usize = 2;
const DT_PLTGOT: usize = 3;
const DT_SYMTAB: usize = 6;
const DT_RELA: usize = 7;
const DT_RELASZ: usize = 8;
const DT_RELAENT: usize = 9;
const DT_RPATH: usize = 15;
const DT_PLTREL: usize = 20;
const DT_JMPREL: usize = 23;
const DT_RUNPATH: usize = 29;
const DT_FLAGS: usize = 30;
const DT_NUM: usize = 35;
const DT_LOPROC: usize = 0x70000000;
const DT_THISPROCNUM: usize = 0; // x86_64
const DT_VERNEEDNUM: usize = 0x6fffffff;
const DT_VERSIONTAGNUM: usize = 16;
const DT_EXTRANUM: usize = 3;
const DT_VALRNGHI: usize = 0x6ffffdff;
const DT_VALNUM: usize = 12;
const DT_ADDRRNGHI: usize = 0x6ffffeff;
const DT_ADDRNUM: usize = 11;
const DT_GNU_PRELINKED: usize = 0x6ffffdf5;

const DT_VERSYM: usize = 0x6ffffff0;

const DT_RELACOUNT: usize = 0x6ffffff9;
const DT_FLAGS_1: usize = 0x6ffffffb;

const DF_SYMBOLIC: Elf64_Xword = 0x00000002;
const DF_BIND_NOW: Elf64_Xword = 0x00000008;

const DF_1_NOW: Elf64_Xword = 0x00000001;

const R_X86_64_NONE: u32 = 0;
const R_X86_64_GLOB_DAT: u32 = 6; /* Create GOT entry */
const R_X86_64_JUMP_SLOT: u32 = 7; /* Create PLT entry */
const R_X86_64_RELATIVE: u32 = 8;
const R_X86_64_DTPMOD64: u32 = 16;	/* ID of module containing symbol */
const R_X86_64_DTPOFF64: u32 = 17;	/* Offset in module's TLS block */
const R_X86_64_TPOFF64: u32 =	18;	/* Offset in initial TLS block */
const R_X86_64_TLSDESC: u32 = 36;	/* TLS descriptor.  */

const SHN_UNDEF: Elf64_Section = 0; /* Undefined section */
const SHN_ABS: Elf64_Section = 0xfff1; /* Associated symbol is absolute */

const STT_GNU_IFUNC: u8 = 10; /* Symbol is indirect code object */

#[inline]
#[allow(non_snake_case)]
fn DT_VERSIONTAGIDX(tag: usize) -> usize {
    DT_VERNEEDNUM.wrapping_sub(tag)
}

#[inline]
#[allow(non_snake_case)]
fn VERSYMIDX(sym: usize) -> usize {
    DT_NUM + DT_THISPROCNUM + DT_VERSIONTAGIDX(sym)
}

#[inline]
#[allow(non_snake_case)]
fn DT_EXTRATAGIDX(tag: usize) -> usize {
    (-((tag as i32) << 1 >> 1) - 1) as usize
}

#[inline]
#[allow(non_snake_case)]
fn DT_VALTAGIDX(tag: usize) -> usize {
    DT_VALRNGHI.wrapping_sub(tag)
}

#[inline]
#[allow(non_snake_case)]
fn DT_ADDRTAGIDX(tag: usize) -> usize {
    DT_ADDRRNGHI.wrapping_sub(tag)
}

#[inline]
#[allow(non_snake_case)]
fn VALIDX(tag: usize) -> usize {
    DT_NUM + DT_THISPROCNUM + DT_VERSIONTAGNUM + DT_EXTRANUM + DT_VALTAGIDX(tag)
}

#[repr(C)]
union Elf64_Dyn_value {
    d_val: Elf64_Xword, /* Integer value */
    d_ptr: Elf64_Addr,  /* Address value */
}

#[repr(C)]
struct Elf64_Dyn {
    d_tag: usize, /* Dynamic entry type */
    d_un: Elf64_Dyn_value,
}

#[repr(C)]
struct Elf64_Rela {
    r_offset: Elf64_Addr,   /* Address */
    r_info: Elf64_Xword,    /* Relocation type and symbol index */
    r_addend: Elf64_Sxword, /* Addend */
}

const L_INFO_LEN: usize =
    (DT_NUM + DT_THISPROCNUM + DT_VERSIONTAGNUM + DT_EXTRANUM + DT_VALNUM + DT_ADDRNUM) as usize;

#[allow(non_camel_case_types)]
struct link_map_machine {
    plt: Elf64_Addr,    /* Address of .plt + 0x16 */
    gotplt: Elf64_Addr, /* Address of .got + 0x18 */
    tlsdesc_table: *const u8,
}

#[repr(C)]
struct link_map {
    l_addr: Elf64_Addr,
    // char *l_name
    l_ld: *const Elf64_Dyn,

    l_info: [*const Elf64_Dyn; L_INFO_LEN],

    l_versions: *const r_found_version,

    // other fields
    l_relocated: u8,
    l_ld_readonly: u8,

    l_flags_1: Elf64_Word,
    l_flags: Elf64_Word,

    l_mach: link_map_machine,

    l_tls_offset: isize,
}

#[allow(non_camel_case_types)]
struct r_scope_elem {}

#[inline]
fn dl_relocate_ld(l: &link_map) -> bool {
    l.l_ld_readonly != 0 || DL_RO_DYN_SECTION != 0
}

#[inline]
#[allow(non_snake_case)]
fn D_PTR(map: &link_map, i: usize) -> Elf64_Addr {
    (unsafe { (*map.l_info[i]).d_un.d_ptr }) + (if dl_relocate_ld(map) { 0 } else { map.l_addr })
}

#[no_mangle]
fn memcpy() {
    panic!("memcpy");
}

#[inline]
fn elf_get_dynamic_info(l: &mut link_map, bootstrap: bool, static_pie_bootstrap: bool) {
    let mut _dyn = l.l_ld;
    while unsafe { (*_dyn).d_tag } != DT_NULL {
        let tag = unsafe { (*_dyn).d_tag };
        let i: d_tag_utype = if (tag as d_tag_utype) < DT_NUM as d_tag_utype {
            tag as d_tag_utype
        } else if tag >= DT_LOPROC && tag < DT_LOPROC + DT_THISPROCNUM {
            (tag - DT_LOPROC + DT_NUM) as d_tag_utype
        } else if (DT_VERSIONTAGIDX(tag) as d_tag_utype) < DT_VERSIONTAGNUM as d_tag_utype {
            VERSYMIDX(tag) as d_tag_utype
        } else if (DT_EXTRATAGIDX(tag) as d_tag_utype) < DT_EXTRANUM as d_tag_utype {
            (DT_EXTRATAGIDX(tag) + DT_NUM + DT_THISPROCNUM + DT_VERSIONTAGNUM) as d_tag_utype
        } else if (DT_VALTAGIDX(tag) as d_tag_utype) < DT_VALNUM as d_tag_utype {
            (DT_VALTAGIDX(tag) + DT_NUM + DT_THISPROCNUM + DT_VERSIONTAGNUM + DT_EXTRANUM)
                as d_tag_utype
        } else if (DT_ADDRTAGIDX(tag) as d_tag_utype) < DT_ADDRNUM as d_tag_utype {
            (DT_ADDRTAGIDX(tag)
                + DT_NUM
                + DT_THISPROCNUM
                + DT_VERSIONTAGNUM
                + DT_EXTRANUM
                + DT_VALNUM) as d_tag_utype
        } else {
            continue;
        };
        l.l_info[i as usize] = _dyn;
        _dyn = unsafe { _dyn.add(1) };
    }

    if l.l_info[DT_PLTREL] != core::ptr::null() {
        unsafe {
            assert!((*l.l_info[DT_PLTREL]).d_un.d_val == DT_RELA as u64);
        };
    }

    if l.l_info[DT_RELA as usize] != core::ptr::null() {
        unsafe {
            assert!(
                (*l.l_info[DT_RELAENT]).d_un.d_val == core::mem::size_of::<Elf64_Rela>() as u64
            );
        };
    }

    if bootstrap || static_pie_bootstrap {
        assert!(l.l_info[DT_RUNPATH] == core::ptr::null());
        assert!(l.l_info[DT_RPATH] == core::ptr::null());
    }

    if bootstrap {
        assert!(
            l.l_info[VERSYMIDX(DT_FLAGS_1) as usize] == core::ptr::null()
                || (unsafe { (*l.l_info[VERSYMIDX(DT_FLAGS_1) as usize]).d_un.d_val } & !DF_1_NOW)
                    == 0
        );
        assert!(
            l.l_info[DT_FLAGS] == core::ptr::null()
                || (unsafe { (*l.l_info[DT_FLAGS]).d_un.d_val } & !DF_BIND_NOW) == 0
        );
    } else {
        if l.l_info[DT_FLAGS] != core::ptr::null() {
            l.l_flags = unsafe { (*l.l_info[DT_FLAGS]).d_un.d_val } as u32;
            // omitted some stuff here
            // filling some info entries to be info[DT_FLAGS]
        }
        if l.l_info[VERSYMIDX(DT_FLAGS_1) as usize] != core::ptr::null() {
            l.l_flags_1 = unsafe { (*l.l_info[VERSYMIDX(DT_FLAGS_1) as usize]).d_un.d_val } as u32;
            // omitted some stuff
            // DF_1_NODELETE
            // warn about unsupported DF_1_* flags
        }
        if l.l_info[DT_RUNPATH] != core::ptr::null() {
            l.l_info[DT_RPATH] = core::ptr::null();
        }
    }
}

// unsafe fn elf_machine_runtime_setup(
//     l: &mut link_map,
//     scope: *const r_scope_elem,
//     lazy: i32,
//     profile: i32,
// ) -> i32 {
//     if l.l_info[DT_JMPREL] != core::ptr::null() && lazy != 0 {
//         let got = D_PTR(l, DT_PLTGOT) as *mut Elf64_Addr;
//         if *got.offset(1) != 0 {
//             l.l_mach.plt = *got.offset(1) + l.l_addr;
//             l.l_mach.gotplt = got.offset(3) as Elf64_Addr;
//         }
//         *(got.offset(1)) = l as *const link_map as Elf64_Addr;
//     }
//     // panic!("TODO");
//     0
// }

#[derive(Default)]
struct Range {
    start: Elf64_Addr,
    size: Elf64_Addr,
    nrelative: Elf64_Xword,
    lazy: i32,
}

#[allow(non_camel_case_types)]
type Elf64_Section = u16;

#[repr(C)]
struct Elf64_Sym {
    st_name: Elf64_Word,     /* Symbol name (string tbl index) */
    st_info: u8,             /* Symbol type and binding */
    st_other: u8,            /* Symbol visibility */
    st_shndx: Elf64_Section, /* Section index */
    st_value: Elf64_Addr,    /* Symbol value */
    st_size: Elf64_Xword,    /* Symbol size */
}

#[inline]
fn ELF64_R_TYPE(i: Elf64_Xword) -> u32 {
    (i & 0xffffffff) as u32
}

#[inline]
fn ELF64_R_SYM(i: Elf64_Xword) -> usize {
    (i >> 32) as usize
}

#[inline]
unsafe fn elf_machine_rela_relative(
    l_addr: Elf64_Addr,
    reloc: *const Elf64_Rela,
    reloc_addr_arg: *const Elf64_Addr,
) {
    let reloc_addr = reloc_addr_arg as *mut Elf64_Addr;
    assert!(ELF64_R_TYPE((*reloc).r_info) == R_X86_64_RELATIVE);
    (*reloc_addr) = (l_addr as i64 + (*reloc).r_addend) as u64;
}

#[allow(non_camel_case_types)]
type Elf64_Half = u16;

#[allow(non_camel_case_types)]
struct r_found_version {}

#[inline]
fn LOOKUP_VALUE_ADDRESS(map: &link_map, set: bool) -> Elf64_Addr {
    map.l_addr
}

// rtld.c ELF_DYNAMIC_RELOCATE
// dynamic-link.h ELF_DYNAMIC_DO_RELA _ELF_DYNAMIC_DO_RELOC elf_dynamic_do_Rela
// do-rel.h elf_machine_rela
// sysdeps/x86_64/dl-machine.h SYMBOL_ADDRESS
// sysdeps/generic/ldsodefs.h
#[inline]
fn SYMBOL_ADDRESS(map: &link_map, reff: &Elf64_Sym, map_set: bool) -> Elf64_Addr {
    (if reff.st_shndx == SHN_ABS {
        0
    } else {
        LOOKUP_VALUE_ADDRESS(map, map_set)
    }) + reff.st_value
}

#[inline]
fn ELF64_ST_TYPE(val: u8) -> u8 {
    val & 0xf
}

#[allow(non_camel_case_types)]
union tlsdesc_u1 {
    entry: unsafe extern "C" fn(*const tlsdesc) -> isize,
    entry_slot: u64,
}

#[allow(non_camel_case_types)]
union tlsdesc_u2 {
    arg: Elf64_Addr,
    arg_slot: u64,
}

/* Type used to represent a TLS descriptor in the GOT.  */
#[allow(non_camel_case_types)]
struct tlsdesc
{
    /* Anonymous union is used here to ensure that GOT entry slot is always
    8 bytes for both x32 and x86-64.  */
    u1: tlsdesc_u1,
    u2: tlsdesc_u2,
}

fn _dl_reloc_bad_type(map: &link_map, typ: u32, plt: i32) {}

// dl-machine.h
#[inline]
unsafe fn elf_machine_rela(
    map: &link_map,
    scope: *const r_scope_elem,
    reloc: &Elf64_Rela,
    sym: &Elf64_Sym,
    version: *const r_found_version,
    reloc_addr_arg: Elf64_Addr,
    skip_ifunc: i32,
) {
    let reloc_addr = reloc_addr_arg as *mut Elf64_Addr;
    let r_type = ELF64_R_TYPE(reloc.r_info);

    if r_type == R_X86_64_NONE {
        return;
    } else {
        let sym_map = map; // RESOLVE_MAP(map, scope, sym, version, r_type);
        let mut value: Elf64_Addr = SYMBOL_ADDRESS(sym_map, sym, true);

        if ELF64_ST_TYPE(sym.st_info) == STT_GNU_IFUNC
            && sym.st_shndx != SHN_UNDEF
            && skip_ifunc == 0
        {
            value = core::mem::transmute::<Elf64_Addr, fn() -> Elf64_Addr>(value)();
        }
        match r_type {
            R_X86_64_GLOB_DAT | R_X86_64_JUMP_SLOT => *reloc_addr = value + reloc.r_addend as u64,
            R_X86_64_DTPMOD64 => *reloc_addr = 1,
            R_X86_64_DTPOFF64 => (),
            R_X86_64_TLSDESC => {
                let td = &mut *(reloc_addr as *mut tlsdesc);
                td.u2.arg = sym.st_value - (sym_map.l_tls_offset as u64) + (reloc.r_addend as u64);
                td.u1.entry = _dl_tlsdesc_return;
            },
            R_X86_64_TPOFF64 => {
                value = sym.st_value + reloc.r_addend - sym_map.l_tls_offset;
                *reloc_addr = value;
            },

            // R_x86_64_PC32 => {
            //     value += reloc.r_addend - reloc_addr;
            //     *(reloc_addr as *mut u32) = value;
            //     assert!(value as u32 as u64 == value);
            // },
            // R_X86_64_COPY => {
            //     memcpy(reloc_addr_arg, value, )
            // }
            _ => _dl_reloc_bad_type(map, r_type, 0),
        }
    }
}

#[inline]
unsafe fn elf_dynamic_do_Rela(
    map: &link_map,
    scope: *const r_scope_elem,
    reladdr: Elf64_Addr,
    relsize: Elf64_Addr,
    nrelative: Elf64_Xword,
    lazy: i32,
    skip_ifunc: i32,
) {
    let mut r = reladdr as *const Elf64_Rela;
    let end = (reladdr + relsize) as *const Elf64_Rela;
    let l_addr = map.l_addr;

    let symtab = D_PTR(map, DT_SYMTAB) as *const Elf64_Sym;
    let mut relative = r;
    r = r.offset(nrelative as isize);
    while relative < r {
        elf_machine_rela_relative(
            l_addr,
            relative,
            (l_addr + (*relative).r_offset) as *const Elf64_Addr,
        );
        relative = relative.offset(1);
    }
    assert!(map.l_info[VERSYMIDX(DT_VERSYM)] != core::ptr::null());

    let version = D_PTR(map, VERSYMIDX(DT_VERSYM)) as *const Elf64_Half;
    while r < end {
        let ndx = (*version.add(ELF64_R_SYM((*r).r_info))) & 0x7fff;
        let sym = symtab.add(ELF64_R_SYM((*r).r_info));
        let r_addr_arg = l_addr + (*r).r_offset;
        let rversion = map.l_versions.add(ndx as usize);
        elf_machine_rela(map, scope, &(*r), &(*sym), rversion, r_addr_arg, skip_ifunc);

        r = r.offset(1);
    }
}

unsafe fn _ELF_DYNAMIC_DO_RELOC(
    map: &link_map,
    scope: *const r_scope_elem,
    do_lazy: i32,
    skip_ifunc: i32,
) {
    let mut ranges: [Range; 2] = [Default::default(), Default::default()];

    if map.l_info[DT_RELA] != core::ptr::null() {
        // range of the type R_X86_64_RELATIVE relocations
        ranges[0].start = D_PTR(map, DT_RELA);
        ranges[0].size = (*map.l_info[DT_RELASZ]).d_un.d_val;
        if map.l_info[VERSYMIDX(DT_RELACOUNT)] != core::ptr::null() {
            ranges[0].nrelative = (*map.l_info[VERSYMIDX(DT_RELACOUNT)]).d_un.d_val;
        }
    }
    if map.l_info[DT_PLTREL] != core::ptr::null() {
        let start = D_PTR(map, DT_JMPREL);
        let size = (*map.l_info[DT_PLTRELSZ]).d_un.d_val;
        if ranges[0].start + ranges[0].size == start + size {
            // This is the case DT_JMPREL occupies the tail end of DT_RELA
            // Subtract the DT_JMPREL portion from DT_RELA
            ranges[0].size -= size;
        }
        if do_lazy == 0 && ranges[0].start + ranges[0].size == start {
            // This is the case DT_JMPREL starts immediately after DT_RELA
            // One range suffices in this case.
            ranges[0].size += size;
        } else {
            // There is a gap between DT_JMPREL and DT_RELA
            ranges[1].start = start;
            ranges[1].size = size;
            ranges[1].lazy = do_lazy;
        }
    }
    for range in &ranges {
        elf_dynamic_do_Rela(
            map,
            scope,
            range.start,
            range.size,
            range.nrelative,
            range.lazy,
            skip_ifunc,
        );
    }
}

unsafe fn ELF_DYNAMIC_RELOCATE(
    map: &mut link_map,
    scope: *const r_scope_elem,
    lazy: i32,
    consider_profile: i32,
    skip_ifunc: i32,
) {
    let edr_lazy = 0; //elf_machine_runtime_setup(map, scope, lazy, consider_profile);
                      // ELF_DYNAMIC_DO_REL(map, scope, edr_lazy, skip_ifunc);
                      // ELF_DYNAMIC_DO_RELA(map, scope, edr_lazy, skip_ifunc);
                      // _ELF_DYNAMIC_DO_RELOC (RELA, Rela, map, scope, lazy, skip_ifunc, _ELF_CHECK_REL)
    _ELF_DYNAMIC_DO_RELOC(map, scope, lazy, skip_ifunc);
}

#[no_mangle]
unsafe fn _start() {
    // let mut dl_rtld_map: link_map = link_map {
    //     l_addr: 0,
    //     l_ld: 0 as *const _,
    //     l_info: [0 as *const _; L_INFO_LEN],
    //     l_ld_readonly: 0,
    // };
    dl_rtld_map.l_addr = elf_machine_load_address();
    dl_rtld_map.l_ld = (dl_rtld_map.l_addr + elf_machine_dynamic()) as *const Elf64_Dyn;
    dl_rtld_map.l_ld_readonly = DL_RO_DYN_SECTION;
    elf_get_dynamic_info(&mut dl_rtld_map, true, false);

    if dl_rtld_map.l_addr != 0
        || dl_rtld_map.l_info[VALIDX(DT_GNU_PRELINKED) as usize] == core::ptr::null()
    {
        ELF_DYNAMIC_RELOCATE(&mut dl_rtld_map, core::ptr::null(), 0, 0, 0);
    }
    dl_rtld_map.l_relocated = 1;
    exit(0);
    // println!("Hello, world!");
}

#[lang = "eh_personality"]
fn eh_personality() {}

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    loop {}
}

// reminder: `!` is the `never` type - this indicates
// that `exit` never returns.
pub unsafe fn exit(code: i32) -> ! {
    let syscall_number: u64 = 60;
    asm!(
        "syscall",
        in("rax") syscall_number,
        in("rdi") code,
        options(noreturn)
    );
}

#[allow(non_upper_case_globals)]
static mut dl_rtld_map: link_map = unsafe { MaybeUninit::zeroed().assume_init() };

// static mut SYS_EXIT: u64 = 60;
