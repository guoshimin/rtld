#include <stdio.h>

extern char _DYNAMIC[] __attribute__((visibility("hidden")));

int main() {
  printf("%p\n", &_DYNAMIC);
  printf("%p\n", _DYNAMIC);
  return 0;
}
